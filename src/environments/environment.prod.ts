export const environment = {
  production: true,
  apis: {
    theCatApi: {
      host: 'https://api.thecatapi.com/',
      api: 'v1/',
      apiKey: '95597ddf-f5e2-41f4-aae6-7362f922303d',
    },
  },
};
