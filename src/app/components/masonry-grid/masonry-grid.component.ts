import { Component, EventEmitter, Input, Output } from '@angular/core';

import { MasonryItemInterface } from './models/interfaces/masonry-item.interface';

@Component({
  selector: 'app-masonry-grid',
  templateUrl: './masonry-grid.component.html',
  styleUrls: ['./masonry-grid.component.scss'],
})
export class MasonryGridComponent {
  @Input()
  public items: MasonryItemInterface[];

  @Output()
  public onClick: EventEmitter<MasonryItemInterface>;

  constructor() {
    this.items = [];
    this.onClick = new EventEmitter<MasonryItemInterface>();
  }

  public onClickItem(item: MasonryItemInterface): void {
    this.onClick.emit(item);
  }
}
