import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasonryGridComponent } from './masonry-grid.component';

import { MasonryItemInterface } from './models/interfaces/masonry-item.interface';

const MOCKED_ITEM = { title: 'test', image: 'test', attributes: [] } as MasonryItemInterface;

describe('MasonryGridComponent', () => {
  let component: MasonryGridComponent;
  let fixture: ComponentFixture<MasonryGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MasonryGridComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MasonryGridComponent);
    component = fixture.componentInstance;
  });

  it('should create the masonry grid component', () => {
    expect(component).toBeTruthy();
  });

  describe('onClickItem method test suite', () => {
    it('should emit an item event correctly', () => {
      spyOn(component.onClick, 'emit').and.callThrough().and.stub();
      component.onClickItem({ ...MOCKED_ITEM });
      expect(component.onClick.emit).toHaveBeenCalledTimes(1);
    });
  });
});
