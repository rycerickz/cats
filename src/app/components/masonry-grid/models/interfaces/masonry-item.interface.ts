import { MasonryItemAttributeInterface } from './masonry-item-attribute.interface';

export interface MasonryItemInterface {
  title: string;
  image: string;
  attributes: MasonryItemAttributeInterface[];
}
