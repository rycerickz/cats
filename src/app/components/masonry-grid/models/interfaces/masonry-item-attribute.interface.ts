export interface MasonryItemAttributeInterface {
  icon: string;
  flag: string;
  label: string;
  value: string;
}
