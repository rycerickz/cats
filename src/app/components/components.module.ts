import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NavComponent } from './nav/nav.component';
import { MasonryGridComponent } from './masonry-grid/masonry-grid.component';
import { StarsComponent } from './stars/stars.component';

@NgModule({
  imports: [CommonModule, RouterModule, NgbModule],
  declarations: [NavComponent, MasonryGridComponent, StarsComponent],
  exports: [NavComponent, MasonryGridComponent, StarsComponent],
})
export class ComponentsModule {}
