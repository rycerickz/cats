import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-starts',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss'],
})
export class StarsComponent {
  @Input()
  public repeat: number = 0;

  public readonly MAXIMUM = 5;
}
