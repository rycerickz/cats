import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MockModule } from 'ng-mocks';

import { NavComponent } from './nav.component';

@NgModule({
  imports: [RouterModule, NgbModule],
  declarations: [NavComponent],
  exports: [NavComponent],
})
class MockedModule {}

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(MockedModule)],
      declarations: [NavComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
  });

  it('should create the nav component', () => {
    expect(component).toBeTruthy();
  });
});
