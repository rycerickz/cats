import { Component } from '@angular/core';

import { LinkInterface } from '../../models/interfaces/app/link.interface';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  public isCollapsed: boolean;
  public links: LinkInterface[];

  constructor() {
    this.isCollapsed = true;
    this.links = [
      { label: 'Breeds', url: '/pages/breeds' },
      { label: 'Categories', url: '/pages/categories' },
    ];
  }
}
