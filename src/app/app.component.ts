import { Component, HostListener, OnDestroy } from '@angular/core';

import { ScrollService } from './services/scroll.service';

import { ViewTemplateComponent } from './models/templates/view-template.component';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent extends ViewTemplateComponent implements OnDestroy {
  @HostListener('scroll', ['$event'])
  public onScroll(event: Event): void {
    const target: HTMLInputElement = event.target! as HTMLInputElement;
    if (target.offsetHeight + target.scrollTop >= target.scrollHeight) {
      this._scrollService.scrollToBottom$.next(true);
    }
  }

  public spinnerBackgroundColor: string = 'rgba(52, 58, 64, .5)';

  constructor(private readonly _scrollService: ScrollService) {
    super();
  }

  public ngOnDestroy(): void {
    this.onUnsubscribeAll();
  }
}
