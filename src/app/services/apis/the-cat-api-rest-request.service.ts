import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';

import { RestRequestTemplateService } from '../../models/templates/rest-request-template.service';

import { Observable } from 'rxjs';
import { BreedInterface } from '../../models/interfaces/the-cat-api/breed.interface';
import { CategoryInterface } from '../../models/interfaces/the-cat-api/category.interface';
import { ImageInterface } from '../../models/interfaces/the-cat-api/image.interface';

@Injectable({ providedIn: 'root' })
export class TheCatApiRestRequestService extends RestRequestTemplateService {
  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.httpHeaders = this.headers;
  }

  protected get path(): string {
    return environment.apis.theCatApi.host + environment.apis.theCatApi.api;
  }

  protected get headers(): HttpHeaders {
    return new HttpHeaders({
      'x-api-key': `${environment.apis.theCatApi.apiKey}`,
      'Content-Type': 'application/json',
    });
  }

  protected get(url: string, query: any = {}): Observable<any> {
    return super.get(this.path + url + this.object2Query(query));
  }

  protected post(url: string, body: any): Observable<any> {
    return super.post(this.path + url, body);
  }

  protected put(url: string, body: any): Observable<any> {
    return super.put(this.path + url, body);
  }

  public getSearches(query: any = {}): Observable<ImageInterface[]> {
    return this.get(`images/search`, query) as Observable<ImageInterface[]>;
  }

  public getCategories(query: any = {}): Observable<CategoryInterface[]> {
    return this.get(`categories`, query) as Observable<CategoryInterface[]>;
  }

  public getBreeds(query: any = {}): Observable<BreedInterface[]> {
    return this.get(`breeds`, query) as Observable<BreedInterface[]>;
  }

  public getBreed(query: any = {}): Observable<BreedInterface[]> {
    return this.get(`breeds/search`, query) as Observable<BreedInterface[]>;
  }

  public getImage(idImage: string, query: any = {}): Observable<ImageInterface> {
    return this.get(`images/${idImage}`, query) as Observable<ImageInterface>;
  }
}
