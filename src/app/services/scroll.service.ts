import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ScrollService {
  public readonly scrollToBottom$: BehaviorSubject<boolean>;

  constructor() {
    this.scrollToBottom$ = new BehaviorSubject<boolean>(false);
  }
}
