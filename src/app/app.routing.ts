import { Routes } from '@angular/router';

export const APP_ROUTES: Routes = [
  {
    path: 'pages',
    loadChildren: () => import('./pages/pages.module').then((module: any) => module.PagesModule),
  },
  {
    path: '',
    redirectTo: '/pages/breeds',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/pages/breeds',
  },
];
