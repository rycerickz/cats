import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BehaviorSubject } from 'rxjs';
import { NgxSpinnerModule } from 'ngx-spinner';

import { MockModule } from 'ng-mocks';

import { ScrollService } from './services/scroll.service';

import { AppComponent } from './app.component';

import { NavComponent } from './components/nav/nav.component';

@NgModule({
  imports: [RouterModule, NgxSpinnerModule],
  declarations: [NavComponent],
  exports: [NavComponent],
})
class MockedModule {}

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(MockedModule)],
      declarations: [AppComponent],
      providers: [
        {
          provide: ScrollService,
          useValue: {
            scrollToBottom$: new BehaviorSubject<boolean>(false),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should create the app component', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnDestroy method test suite', () => {
    it('should unsubscribe from all observables correctly', () => {
      spyOn(component, 'onUnsubscribeAll').and.callThrough().and.stub();
      component.ngOnDestroy();
      expect(component.onUnsubscribeAll).toHaveBeenCalledTimes(1);
    });
  });

  describe('onScroll method test suite', () => {
    it('should call the next method when this is bottom', inject([ScrollService], (scrollService: ScrollService) => {
      spyOn(scrollService.scrollToBottom$, 'next').and.callThrough();
      component.onScroll({
        target: {
          offsetHeight: 1,
          scrollTop: 0,
          scrollHeight: 0,
        } as HTMLInputElement,
      } as unknown as Event);
      expect(scrollService.scrollToBottom$.next).toHaveBeenCalledTimes(1);
    }));

    it("shouldn't call next method when this is not bottom", inject([ScrollService], (scrollService: ScrollService) => {
      spyOn(scrollService.scrollToBottom$, 'next').and.callThrough();
      component.onScroll({
        target: {
          offsetHeight: 0,
          scrollTop: 0,
          scrollHeight: 1,
        } as HTMLInputElement,
      } as unknown as Event);
      expect(scrollService.scrollToBottom$.next).toHaveBeenCalledTimes(0);
    }));
  });
});
