import { Subject } from 'rxjs';

import { environment } from '../../../environments/environment';

export class ViewTemplateComponent {
  public readonly ENVIROMENT = environment;

  private readonly _unsubscribeAll: Subject<any>;

  constructor() {
    this._unsubscribeAll = new Subject();
  }

  protected get unsubscribeAll(): Subject<any> {
    return this._unsubscribeAll;
  }

  public onUnsubscribeAll(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }
}
