import { HttpClient, HttpHeaders } from '@angular/common/http';

import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

export class RestRequestTemplateService {
  private _httpHeaders: HttpHeaders | undefined;
  private _handleResponse: ((response: any) => any) | undefined;
  private _handleError: ((error: any) => any) | undefined;

  constructor(protected readonly _httpClient: HttpClient) {}

  private static processingResponse(response: any): any {
    return response;
  }

  private static processingError(error: any): any {
    return error;
  }

  protected get handleResponse(): (response: any) => any {
    if (!this._handleResponse) {
      this._handleResponse = RestRequestTemplateService.processingResponse;
    }
    return this._handleResponse;
  }

  protected set handleResponse(value: (response: any) => any) {
    this._handleResponse = value;
  }

  protected get handleError(): (error: any) => any {
    if (!this._handleError) {
      this._handleError = RestRequestTemplateService.processingError;
    }
    return this._handleError;
  }

  protected set handleError(value: (error: any) => any) {
    this._handleError = value;
  }

  protected get httpHeaders(): HttpHeaders {
    if (!this._httpHeaders) {
      this._httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
      });
    }
    return this._httpHeaders;
  }

  protected set httpHeaders(value: HttpHeaders) {
    this._httpHeaders = value;
  }

  protected object2Query(query: any): string {
    return (
      '?' +
      Object.keys(query)
        .map((key: string) => `${key}=${query[key]}`)
        .join('&')
    );
  }

  protected get(url: string): Observable<any> {
    return this._httpClient.get(url, { headers: this.httpHeaders }).pipe(
      map((response: any) => this.handleResponse(response)),
      catchError(this.handleError)
    );
  }

  protected post(url: string, body: any): Observable<any> {
    return this._httpClient.post(url, body, { headers: this.httpHeaders }).pipe(
      map((response: any) => this.handleResponse(response)),
      catchError(this.handleError)
    );
  }

  protected put(url: string, body: any): Observable<any> {
    return this._httpClient.put(url, body, { headers: this.httpHeaders }).pipe(
      map((response: any) => this.handleResponse(response)),
      catchError(this.handleError)
    );
  }

  protected delete(url: string): Observable<any> {
    return this._httpClient.delete(url, { headers: this.httpHeaders }).pipe(
      map((response: any) => this.handleResponse(response)),
      catchError(this.handleError)
    );
  }
}
