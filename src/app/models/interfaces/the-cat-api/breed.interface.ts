import { ImageInterface } from './image.interface';

export interface BreedInterface {
  id: string;
  reference_image_id: string;
  name: string;
  temperament: string;
  description: string;
  origin: string;
  life_span: string;
  wikipedia_url: string;
  affection_level: number;
  adaptability: number;
  child_friendly: number;
  dog_friendly: number;
  energy_level: number;
  grooming: number;
  health_issues: number;
  intelligence: number;
  shedding_level: number;
  social_needs: number;
  stranger_friendly: number;
  vocalisation: number;
  country_code: string;
  image: ImageInterface | undefined;
}
