export interface ImageInterface {
  id: string;
  url: string;
  width: number;
  height: number;
}
