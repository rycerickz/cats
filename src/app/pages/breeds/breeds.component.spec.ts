import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';

import { BehaviorSubject, of } from 'rxjs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

import { MockModule } from 'ng-mocks';

import { ScrollService } from '../../services/scroll.service';
import { TheCatApiRestRequestService } from '../../services/apis/the-cat-api-rest-request.service';

import { BreedsComponent } from './breeds.component';
import { MasonryGridComponent } from '../../components/masonry-grid/masonry-grid.component';

import { MasonryItemInterface } from '../../components/masonry-grid/models/interfaces/masonry-item.interface';
import { MasonryItemAttributeInterface } from '../../components/masonry-grid/models/interfaces/masonry-item-attribute.interface';
import { BreedInterface } from '../../models/interfaces/the-cat-api/breed.interface';
import { ImageInterface } from '../../models/interfaces/the-cat-api/image.interface';

const MOCKED_BREED = {
  id: '1',
  name: 'test',
  origin: 'test',
  country_code: 'test',
  life_span: 'test',
  image: { url: 'test' } as ImageInterface
} as BreedInterface;

const MOCKED_ITEM = {
  title: 'test',
  image: 'test',
  attributes: [
    {
      icon: 'la-globe',
      label: 'Origin',
      value: 'test',
      flag: 'test'
    },
    {
      icon: 'la-heartbeat',
      label: 'Life Span',
      value: 'test'
    }
  ] as MasonryItemAttributeInterface[]
} as MasonryItemInterface;

const MOCKED_FILTER = { name: '', origin: '', limit: 16 };

@NgModule({
  imports: [ReactiveFormsModule, NgbModule],
  declarations: [MasonryGridComponent],
  exports: [MasonryGridComponent]
})
class MockedModule {
}

describe('BreedsComponent', () => {
  let component: BreedsComponent;
  let fixture: ComponentFixture<BreedsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(MockedModule)],
      declarations: [BreedsComponent],
      providers: [
        {
          provide: Router,
          useValue: {
            navigate: () => Promise.resolve(true)
          }
        },
        {
          provide: FormBuilder,
          useValue: {
            group: () => ({
              get: () => ({ value: '' }),
              valueChanges: of({ ...MOCKED_FILTER })
            })
          }
        },
        {
          provide: NgxSpinnerService,
          useValue: {
            show: () => {
            },
            hide: () => {
            }
          }
        },
        {
          provide: ScrollService,
          useValue: {
            scrollToBottom$: new BehaviorSubject<boolean>(false)
          }
        },
        {
          provide: TheCatApiRestRequestService,
          useValue: {
            getBreeds: () => of([{ ...MOCKED_BREED }] as BreedInterface[])
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedsComponent);
    component = fixture.componentInstance;
  });

  it('should create the breeds component', () => {
    expect(component).toBeTruthy();
  });

  describe('query property test suite', () => {
    it('should return query property correctly', () => {
      const query = component.query;
      expect(query).toEqual({ limit: 15, page: 0 });
    });
  });

  describe('ngOnInit method test suite', () => {
    it('should call loadBreed method correctly', () => {
      spyOn(component, 'listeningScroll').and.callThrough().and.stub();
      spyOn(component, 'listeningFilter').and.callThrough().and.stub();
      spyOn(component, 'loadBreeds').and.callThrough().and.stub();
      component.ngOnInit();
      expect(component.listeningScroll).toHaveBeenCalledTimes(1);
      expect(component.listeningFilter).toHaveBeenCalledTimes(1);
      expect(component.loadBreeds).toHaveBeenCalledTimes(1);
    });
  });

  describe('ngOnDestroy method test suite', () => {
    it('should unsubscribe from all observables correctly', () => {
      spyOn(component, 'onUnsubscribeAll').and.callThrough().and.stub();
      component.ngOnDestroy();
      expect(component.onUnsubscribeAll).toHaveBeenCalledTimes(1);
    });
  });

  describe('listeningScroll method test suite', () => {
    it('should call loadBreeds method when this is bottom', inject(
      [ScrollService],
      async (scrollService: ScrollService) => {
        component.page = 0;
        spyOn(component, 'loadBreeds').and.callThrough().and.stub();
        component.listeningScroll();
        scrollService.scrollToBottom$.next(true);
        await fixture.whenStable().then(() => {
          expect(component.page).toBe(1);
          expect(component.loadBreeds).toHaveBeenCalledTimes(1);
        });
      }
    ));
  });

  describe('listeningFilter method test suite', () => {
    it('should call loadBreeds method when limit was changed', async () => {
      component.limit = 15;
      component.page = 1;
      spyOn(component, 'filterItems').and.callThrough().and.stub();
      spyOn(component, 'loadBreeds').and.callThrough().and.stub();
      component.listeningFilter();
      component.filter.valueChanges.subscribe();
      await fixture.whenStable().then(() => {
        expect(component.limit).toBe(16);
        expect(component.page).toBe(0);
        expect(component.items).toEqual([]);
        expect(component.itemsBackup).toEqual([]);
        expect(component.filterItems).toHaveBeenCalledTimes(1);
        expect(component.loadBreeds).toHaveBeenCalledTimes(1);
      });
    });

    it('shouldn\'t call loadBreeds method when limit was not changed', async () => {
      component.limit = 16;
      component.page = 1;
      spyOn(component, 'filterItems').and.callThrough().and.stub();
      spyOn(component, 'loadBreeds').and.callThrough().and.stub();
      component.listeningFilter();
      component.filter.valueChanges.subscribe();
      await fixture.whenStable().then(() => {
        expect(component.limit).toBe(16);
        expect(component.page).toBe(1);
        expect(component.filterItems).toHaveBeenCalledTimes(1);
        expect(component.loadBreeds).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('loadBreeds method test suite', () => {
    it('should set the items property correctly', inject(
      [NgxSpinnerService, TheCatApiRestRequestService],
      async (ngxSpinnerService: NgxSpinnerService, theCatApiRestRequestService: TheCatApiRestRequestService) => {
        spyOn(ngxSpinnerService, 'show').and.callThrough().and.stub();
        spyOn(ngxSpinnerService, 'hide').and.callThrough().and.stub();
        spyOn(theCatApiRestRequestService, 'getBreeds').and.callThrough();
        spyOn(component, 'filterBreeds').and.callThrough();
        spyOn(component, 'mapBreeds').and.callThrough();
        spyOn(component, 'filterItems').and.callThrough().and.stub();
        component.loadBreeds();
        await fixture.whenStable().then(() => {
          expect(ngxSpinnerService.show).toHaveBeenCalledTimes(1);
          expect(ngxSpinnerService.hide).toHaveBeenCalledTimes(1);
          expect(theCatApiRestRequestService.getBreeds).toHaveBeenCalledTimes(1);
          expect(component.filterBreeds).toHaveBeenCalledTimes(1);
          expect(component.mapBreeds).toHaveBeenCalledTimes(1);
          expect(component.filterItems).toHaveBeenCalledTimes(1);
          expect(component.items).toEqual([{ ...MOCKED_ITEM }] as MasonryItemInterface[]);
        });
      }
    ));
  });

  describe('filterBreeds method test suite', () => {
    it('should filter breeds correctly when it contains image', () => {
      const breeds: BreedInterface[] = component.filterBreeds([{ ...MOCKED_BREED }] as BreedInterface[]);
      expect(breeds).toEqual([{ ...MOCKED_BREED }] as BreedInterface[]);
    });
    it('should filter breeds correctly when image is missing', () => {
      const breeds: BreedInterface[] = component.filterBreeds([
        {
          ...MOCKED_BREED,
          image: undefined
        }
      ] as BreedInterface[]);
      expect(breeds).toEqual([] as BreedInterface[]);
    });
  });

  describe('mapBreeds method test suite', () => {
    it('should return item list from breeds list', () => {
      const items: MasonryItemInterface[] = component.mapBreeds([{ ...MOCKED_BREED }] as BreedInterface[]);
      expect(items).toEqual([{ ...MOCKED_ITEM }] as MasonryItemInterface[]);
    });
  });

  describe('onClickItem method test suite', () => {
    it('should navigate to another route correctly', inject([Router], async (router: Router) => {
      spyOn(router, 'navigate').and.callThrough();
      return component.onClickItem({ ...MOCKED_ITEM }).then((result: boolean) => {
        expect(result).toBe(true);
        expect(router.navigate).toHaveBeenCalledOnceWith(['/pages/breed', MOCKED_ITEM.title.toLowerCase()]);
      });
    }));
  });

  describe('filterItems method test suite', () => {
    it('should filter items when name filter exist', () => {
      component.filter = {
        get: (text: string) => ({ value: text === 'name' ? 'name' : '' })
      } as FormGroup;
      component.items = [] as MasonryItemInterface[];
      component.itemsBackup = [{ ...MOCKED_ITEM, title: 'name' }] as MasonryItemInterface[];
      component.filterItems();
      expect(component.items).toEqual([{ ...MOCKED_ITEM, title: 'name' }] as MasonryItemInterface[]);
    });

    it('should filter items when origin filter exist', () => {
      component.filter = {
        get: (text: string) => ({ value: text === 'origin' ? 'origin' : '' })
      } as FormGroup;
      component.items = [] as MasonryItemInterface[];
      component.itemsBackup = [
        {
          ...MOCKED_ITEM,
          attributes: [{ ...MOCKED_ITEM.attributes[0], value: 'origin' }]
        }
      ] as MasonryItemInterface[];
      component.filterItems();
      expect(component.items).toEqual([
        {
          ...MOCKED_ITEM,
          attributes: [{ ...MOCKED_ITEM.attributes[0], value: 'origin' }]
        }
      ] as MasonryItemInterface[]);
    });

    it('should filter items when name or origin filter does not exist', () => {
      component.items = [] as MasonryItemInterface[];
      component.itemsBackup = [{ ...MOCKED_ITEM }] as MasonryItemInterface[];
      component.filterItems();
      expect(component.items).toEqual([] as MasonryItemInterface[]);
    });
  });
});
