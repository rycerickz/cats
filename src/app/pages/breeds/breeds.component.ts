import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { debounceTime, map, takeUntil } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { ScrollService } from '../../services/scroll.service';
import { TheCatApiRestRequestService } from '../../services/apis/the-cat-api-rest-request.service';

import { ViewTemplateComponent } from '../../models/templates/view-template.component';

import { MasonryItemInterface } from '../../components/masonry-grid/models/interfaces/masonry-item.interface';
import { MasonryItemAttributeInterface } from '../../components/masonry-grid/models/interfaces/masonry-item-attribute.interface';
import { BreedInterface } from '../../models/interfaces/the-cat-api/breed.interface';

@Component({
  selector: 'app-breeds',
  templateUrl: './breeds.component.html',
  styleUrls: ['./breeds.component.scss'],
})
export class BreedsComponent extends ViewTemplateComponent implements OnInit, OnDestroy {
  public items: MasonryItemInterface[];
  public itemsBackup: MasonryItemInterface[];

  public limits: number[];
  public limit: number;
  public page: number;
  public finalized: boolean;

  public filter: FormGroup;

  public get query(): any {
    return { limit: this.limit, page: this.page };
  }

  constructor(
    private readonly _router: Router,
    private readonly _formBuilder: FormBuilder,
    private readonly _ngxSpinnerService: NgxSpinnerService,
    private readonly _scrollService: ScrollService,
    private readonly _theCatApiRestRequestService: TheCatApiRestRequestService
  ) {
    super();

    this.items = [];
    this.itemsBackup = [];

    this.limits = [15, 30, 45, 60, 75, 90, 100];
    this.limit = 15;
    this.page = 0;
    this.finalized = false;

    this.filter = this._formBuilder.group({
      name: [''],
      origin: [''],
      limit: [this.limit],
    });
  }

  public ngOnInit(): void {
    this.listeningScroll();
    this.listeningFilter();
    this.loadBreeds();
  }

  public ngOnDestroy(): void {
    this.onUnsubscribeAll();
  }

  public listeningScroll(): void {
    this._scrollService.scrollToBottom$.pipe(takeUntil(this.unsubscribeAll)).subscribe((scrollToBottom: boolean) => {
      if (scrollToBottom && !this.finalized) {
        this.page++;
        this.loadBreeds();
      }
    });
  }

  public listeningFilter(): void {
    this.filter.valueChanges.pipe(debounceTime(250), takeUntil(this.unsubscribeAll)).subscribe((value: any) => {
      this.items = [...this.itemsBackup];

      this.filterItems();

      if (value.limit != this.limit) {
        this.limit = value.limit;
        this.page = 0;
        this.items = [];
        this.itemsBackup = [];
        this.loadBreeds();
      }
    });
  }

  public loadBreeds(): void {
    this._ngxSpinnerService.show();
    this._theCatApiRestRequestService
      .getBreeds(this.query)
      .pipe(debounceTime(250), takeUntil(this.unsubscribeAll), map(this.filterBreeds), map(this.mapBreeds))
      .subscribe((items: MasonryItemInterface[]) => {
        this._ngxSpinnerService.hide();
        this.finalized = !items.length;
        this.items = [...this.items, ...items];
        this.itemsBackup = [...items];
        this.filterItems();
      });
  }

  // Se filtran los breeds sin imagenes.
  public filterBreeds(breeds: BreedInterface[]): BreedInterface[] {
    return breeds.filter((breed: BreedInterface) => Boolean(breed.image));
  }

  public mapBreeds(breeds: BreedInterface[]): MasonryItemInterface[] {
    return breeds.map((breed: BreedInterface) => {
      return {
        title: breed.name,
        image: breed.image!.url,
        attributes: [
          {
            icon: 'la-globe',
            label: 'Origin',
            value: breed.origin,
            flag: breed.country_code.toLowerCase(),
          } as MasonryItemAttributeInterface,
          {
            icon: 'la-heartbeat',
            label: 'Life Span',
            value: breed.life_span,
          } as MasonryItemAttributeInterface,
        ] as MasonryItemAttributeInterface[],
      } as MasonryItemInterface;
    });
  }

  public onClickItem(item: MasonryItemInterface): Promise<boolean> {
    return this._router.navigate(['/pages/breed', item.title.toLowerCase()]);
  }

  public filterItems(): void {
    const name = this.filter.get('name')?.value;
    const origin = this.filter.get('origin')?.value;
    if (name || origin) {
      this.items = this.itemsBackup.filter((item: MasonryItemInterface) => {
        if (name && !this.include(item.title, name)) return false;
        return !(origin && !this.include(item.attributes[0].value, origin));
      });
    }
  }

  public include(string1: string, string2: string): boolean {
    if (!string1 || !string2) return false;
    return string1.trim().toLowerCase().includes(string2.trim().toLowerCase());
  }
}
