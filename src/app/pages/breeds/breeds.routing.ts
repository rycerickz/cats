import { Routes } from '@angular/router';

import { BreedsComponent } from './breeds.component';

export const ROUTES: Routes = [
  {
    path: 'breeds',
    component: BreedsComponent,
  },
];
