import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BreedsComponent } from './breeds.component';
import { ROUTES } from './breeds.routing';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NgbModule, RouterModule.forChild(ROUTES), ComponentsModule],
  declarations: [BreedsComponent],
})
export class BreedsModule {}
