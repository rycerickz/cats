import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ROUTES } from './pages.routing';

import { BreedsModule } from './breeds/breeds.module';
import { BreedModule } from './breed/breed.module';
import { CategoriesModule } from './categories/categories.module';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), BreedsModule, BreedModule, CategoriesModule],
})
export class PagesModule {}
