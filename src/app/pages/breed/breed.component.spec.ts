import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { NgModule } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { of } from 'rxjs';

import { MockModule } from 'ng-mocks';

import { TheCatApiRestRequestService } from '../../services/apis/the-cat-api-rest-request.service';

import { BreedComponent } from './breed.component';

import { BreedInterface } from '../../models/interfaces/the-cat-api/breed.interface';
import { ImageInterface } from '../../models/interfaces/the-cat-api/image.interface';

const MOCKED_BREED = { id: '1', reference_image_id: '1', name: 'test' } as BreedInterface;
const MOCKED_IMAGE = { id: '1', url: 'test' } as ImageInterface;

@NgModule({
  imports: [RouterModule, NgbModule],
})
class MockedModule {}

describe('BreedComponent', () => {
  let component: BreedComponent;
  let fixture: ComponentFixture<BreedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(MockedModule)],
      declarations: [BreedComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: (text: string) => {
                  return text;
                },
              },
            },
          },
        },
        {
          provide: NgxSpinnerService,
          useValue: {
            show: () => {},
            hide: () => {},
          },
        },
        {
          provide: TheCatApiRestRequestService,
          useValue: {
            getBreed: () => of([{ ...MOCKED_BREED }] as BreedInterface[]),
            getImage: () => of({ ...MOCKED_IMAGE } as ImageInterface),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedComponent);
    component = fixture.componentInstance;
  });

  it('should create the breed component', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit method test suite', () => {
    it('should call loadBreed method correctly', () => {
      spyOn(component, 'loadBreed').and.callThrough().and.stub();
      component.ngOnInit();
      expect(component.loadBreed).toHaveBeenCalledTimes(1);
    });
  });

  describe('ngOnDestroy method test suite', () => {
    it('should unsubscribe from all observables correctly', () => {
      spyOn(component, 'onUnsubscribeAll').and.callThrough().and.stub();
      component.ngOnDestroy();
      expect(component.onUnsubscribeAll).toHaveBeenCalledTimes(1);
    });
  });

  describe('loadBreed method test suite', () => {
    it('should set the breed property correctly', inject(
      [NgxSpinnerService, TheCatApiRestRequestService],
      async (ngxSpinnerService: NgxSpinnerService, theCatApiRestRequestService: TheCatApiRestRequestService) => {
        spyOn(ngxSpinnerService, 'show').and.callThrough().and.stub();
        spyOn(ngxSpinnerService, 'hide').and.callThrough().and.stub();
        spyOn(theCatApiRestRequestService, 'getBreed').and.callThrough();
        spyOn(component, 'loadImage').and.callThrough().and.stub();
        component.loadBreed();
        await fixture.whenStable().then(() => {
          expect(ngxSpinnerService.show).toHaveBeenCalledTimes(1);
          expect(ngxSpinnerService.hide).toHaveBeenCalledTimes(1);
          expect(theCatApiRestRequestService.getBreed).toHaveBeenCalledTimes(1);
          expect(component.loadImage).toHaveBeenCalledTimes(1);
          expect(component.breed).toEqual({ ...MOCKED_BREED } as BreedInterface);
        });
      }
    ));

    it('should be undefined the breed propery', inject(
      [NgxSpinnerService, TheCatApiRestRequestService],
      async (ngxSpinnerService: NgxSpinnerService, theCatApiRestRequestService: TheCatApiRestRequestService) => {
        spyOn(ngxSpinnerService, 'show').and.callThrough().and.stub();
        spyOn(ngxSpinnerService, 'hide').and.callThrough().and.stub();
        spyOn(theCatApiRestRequestService, 'getBreed').and.callThrough().and.returnValue(of([]));
        spyOn(component, 'loadImage').and.callThrough().and.stub();
        component.loadBreed();
        await fixture.whenStable().then(() => {
          expect(ngxSpinnerService.show).toHaveBeenCalledTimes(1);
          expect(ngxSpinnerService.hide).toHaveBeenCalledTimes(1);
          expect(theCatApiRestRequestService.getBreed).toHaveBeenCalledTimes(1);
          expect(component.loadImage).toHaveBeenCalledTimes(0);
          expect(component.breed).toEqual(undefined);
        });
      }
    ));
  });

  describe('loadImage method test suite', () => {
    it('should set the image property correctly', inject(
      [NgxSpinnerService, TheCatApiRestRequestService],
      async (ngxSpinnerService: NgxSpinnerService, theCatApiRestRequestService: TheCatApiRestRequestService) => {
        component.breed = { ...MOCKED_BREED } as BreedInterface;
        spyOn(ngxSpinnerService, 'show').and.callThrough().and.stub();
        spyOn(ngxSpinnerService, 'hide').and.callThrough().and.stub();
        spyOn(theCatApiRestRequestService, 'getImage').and.callThrough();
        component.loadImage();
        await fixture.whenStable().then(() => {
          expect(ngxSpinnerService.show).toHaveBeenCalledTimes(1);
          expect(ngxSpinnerService.hide).toHaveBeenCalledTimes(1);
          expect(theCatApiRestRequestService.getImage).toHaveBeenCalledTimes(1);
          expect(component.breed!.image).toEqual({ ...MOCKED_IMAGE } as ImageInterface);
        });
      }
    ));
  });
});
