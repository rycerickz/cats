import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { TheCatApiRestRequestService } from '../../services/apis/the-cat-api-rest-request.service';

import { ViewTemplateComponent } from '../../models/templates/view-template.component';

import { BreedInterface } from '../../models/interfaces/the-cat-api/breed.interface';
import { ImageInterface } from '../../models/interfaces/the-cat-api/image.interface';

@Component({
  selector: 'app-breed',
  templateUrl: './breed.component.html',
  styleUrls: ['./breed.component.scss'],
})
export class BreedComponent extends ViewTemplateComponent implements OnInit, OnDestroy {
  public breed: BreedInterface | undefined;

  constructor(
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _ngxSpinnerService: NgxSpinnerService,
    private readonly _theCatApiRestRequestService: TheCatApiRestRequestService
  ) {
    super();
  }

  public ngOnInit(): void {
    this.loadBreed();
  }

  public ngOnDestroy(): void {
    this.onUnsubscribeAll();
  }

  public loadBreed(): void {
    this._ngxSpinnerService.show();
    this._theCatApiRestRequestService
      .getBreed({ q: this._activatedRoute.snapshot.paramMap.get('name') })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((breeds: BreedInterface[]) => {
        this._ngxSpinnerService.hide();
        if (breeds && breeds.length) {
          this.breed = breeds[0];
          this.loadImage();
        }
      });
  }

  public loadImage(): void {
    this._ngxSpinnerService.show();
    this._theCatApiRestRequestService.getImage(this.breed!.reference_image_id).subscribe((image: ImageInterface) => {
      this._ngxSpinnerService.hide();
      this.breed!.image = image;
    });
  }
}
