import { Routes } from '@angular/router';

import { BreedComponent } from './breed.component';

export const ROUTES: Routes = [
  {
    path: 'breed/:name',
    component: BreedComponent,
  },
];
