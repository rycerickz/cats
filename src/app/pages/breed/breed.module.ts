import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BreedComponent } from './breed.component';
import { ROUTES } from './breed.routing';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [CommonModule, NgbModule, RouterModule.forChild(ROUTES), ComponentsModule],
  declarations: [BreedComponent],
})
export class BreedModule {}
