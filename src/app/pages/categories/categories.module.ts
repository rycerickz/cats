import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CategoriesComponent } from './categories.component';
import { ROUTES } from './categories.routing';

import { ComponentsModule } from '../../components/components.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, NgbModule, RouterModule.forChild(ROUTES), ComponentsModule, ReactiveFormsModule],
  declarations: [CategoriesComponent],
})
export class CategoriesModule {}
