import { Routes } from '@angular/router';
import { CategoriesComponent } from './categories.component';

export const ROUTES: Routes = [
  {
    path: 'categories',
    component: CategoriesComponent,
  },
];
