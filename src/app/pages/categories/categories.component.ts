import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { debounceTime, map, takeUntil } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { ScrollService } from '../../services/scroll.service';
import { TheCatApiRestRequestService } from '../../services/apis/the-cat-api-rest-request.service';

import { ViewTemplateComponent } from '../../models/templates/view-template.component';

import { MasonryItemInterface } from '../../components/masonry-grid/models/interfaces/masonry-item.interface';
import { CategoryInterface } from '../../models/interfaces/the-cat-api/category.interface';
import { ImageInterface } from '../../models/interfaces/the-cat-api/image.interface';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent extends ViewTemplateComponent implements OnInit, OnDestroy {
  public items: MasonryItemInterface[];

  public categories: CategoryInterface[];
  public category: CategoryInterface | undefined;

  public limits: number[];
  public limit: number;
  public page: number;
  public finalized: boolean;

  public filter: FormGroup;

  public get query(): any {
    return { limit: this.limit, page: this.page, category_ids: this.category!.id };
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _ngxSpinnerService: NgxSpinnerService,
    private readonly _scrollService: ScrollService,
    private readonly _theCatApiRestRequestService: TheCatApiRestRequestService
  ) {
    super();

    this.items = [];

    this.categories = [];

    this.limits = [15, 30, 45, 60, 75, 90, 100];
    this.limit = 15;
    this.page = 0;
    this.finalized = false;

    this.filter = this._formBuilder.group({
      category: [this.category],
      limit: [this.limit],
    });
  }

  public ngOnInit(): void {
    this.listeningScroll();
    this.listeningFilter();
    this.loadCategories();
  }

  public ngOnDestroy(): void {
    this.onUnsubscribeAll();
  }

  public listeningScroll(): void {
    this._scrollService.scrollToBottom$.pipe(takeUntil(this.unsubscribeAll)).subscribe((scrollToBottom: boolean) => {
      if (scrollToBottom && !this.finalized && this.category) {
        this.page++;
        this.loadSearches();
      }
    });
  }

  public listeningFilter(): void {
    this.filter.valueChanges.pipe(debounceTime(250), takeUntil(this.unsubscribeAll)).subscribe((value: any) => {
      if (value.category != this.category) {
        this.category = value.category;
        this.page = 0;
        this.items = [];
        this.loadCategories();
      }
      if (value.limit != this.limit) {
        this.limit = value.limit;
        this.page = 0;
        this.items = [];
        this.loadCategories();
      }
    });
  }

  public loadCategories(): void {
    this._ngxSpinnerService.show();
    this._theCatApiRestRequestService
      .getCategories()
      .pipe(debounceTime(250), takeUntil(this.unsubscribeAll))
      .subscribe((categories: CategoryInterface[]) => {
        this._ngxSpinnerService.hide();
        this.categories = [...categories];
        this.category = categories[0];
        this.filter.get('category')?.setValue(categories[0]);
        this.loadSearches();
      });
  }

  public loadSearches(): void {
    this._ngxSpinnerService.show();
    this._theCatApiRestRequestService
      .getSearches(this.query)
      .pipe(debounceTime(250), takeUntil(this.unsubscribeAll), map(this.mapSearches))
      .subscribe((items: MasonryItemInterface[]) => {
        this._ngxSpinnerService.hide();
        this.finalized = !items.length;
        this.items = [...this.items, ...items];
      });
  }

  public mapSearches(images: ImageInterface[]): MasonryItemInterface[] {
    return images.map((image: ImageInterface) => {
      return {
        image: image.url,
      } as MasonryItemInterface;
    });
  }

  public onClickItem(item: MasonryItemInterface): void {
    window.open(item.image, '_blank');
  }
}
